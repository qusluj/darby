#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main(int argc, char** argv) {
    puts("diCe random number generator version 0.1");
    int passLength = atoi(argv[1]);
    srand((unsigned)time(NULL));
    for (int i = 0; i <= passLength; i++) {
        numGen();
    }
    return 0;
}
int numGen(void) {
   int a = 0;
   int randomNumber;
   while (a <= 5) {
        randomNumber = rand() % 6 + 1;
        printf("%d",randomNumber);
        a = a + 1;
   }
   printf("\n");
}
